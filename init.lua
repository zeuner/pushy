local modname = "pushy"

pushy = {
}

local random_source = PcgRandom(
    os.time(
    )
)

pushy.get_impulse = function (
    pos
)
    local meta = minetest.get_meta(
        pos
    )
    local impulse_string = meta:get(
        "pushy:impulse"
    )
    if not impulse_string then
        return vector.zero(
        )
    end
    return minetest.deserialize(
        impulse_string
    )
end

pushy.set_impulse = function (
    pos,
    impulse
)
    local meta = minetest.get_meta(
        pos
    )
    if vector.equals(
        vector.zero(
        ),
        impulse
    ) then
        meta:set_string(
            "pushy:impulse",
            ""
        )
    else
        meta:set_string(
            "pushy:impulse",
            minetest.serialize(
                impulse
            )
        )
    end
end

pushy.unpack_impulse = function (
    packed
)
    local result = {
    }
    for k, v in pairs(
        packed
    ) do
        while 0 > v do
            if -1 < v then
                result[
                    #result + 1
                ] = {
                    probability = -v,
                    axis = k,
                    direction = -1,
                }
                v = 0
            else
                result[
                    #result + 1
                ] = {
                    probability = 1,
                    axis = k,
                    direction = -1,
                }
                v = v + 1
            end
        end
        while 0 < v do
            if 1 > v then
                result[
                    #result + 1
                ] = {
                    probability = v,
                    axis = k,
                    direction = 1,
                }
                v = 0
            else
                result[
                    #result + 1
                ] = {
                    probability = 1,
                    axis = k,
                    direction = 1,
                }
                v = v - 1
            end
        end
    end
    return result
end

pushy.pack_impulse = function (
    unpacked
)
    local result = vector.zero(
    )
    for _, v in pairs(
        unpacked
    ) do
        result[
            v.axis
        ] = result[
            v.axis
        ] + v.direction * v.probability
    end
    return result
end

local random_granularity = 65536

local deflection = {
    {
        directions = {
            0,
            0,
        },
        fraction = 0.4,
    },
    {
        directions = {
            1,
            0,
        },
        fraction = 0.15,
    },
    {
        directions = {
            -1,
            0,
        },
        fraction = 0.15,
    },
    {
        directions = {
            0,
            1,
        },
        fraction = 0.15,
    },
    {
        directions = {
            0,
            -1,
        },
        fraction = 0.15,
    },
}

local propagated_fraction = 0.2

minetest.register_abm(
    {
        nodenames = {
            "group:pushy",
        },
        neighbors = {
            "group:pushy",
            "air",
        },
        interval = 0.1,
        chance = 10,
        action = function (
            pos,
            node,
            active_object_count,
            active_object_count_wider
        )
            local impulse_packed = pushy.get_impulse(
                pos
            )
            local impulse_unpacked = pushy.unpack_impulse(
                impulse_packed
            )
            local impulse_unpacked_new = {
            }
            while true do
                if nil == next(
                    impulse_unpacked
                ) then
                    break
                end
                local chosen_index = random_source:next(
                    1,
                    #impulse_unpacked
                )
                local chosen_entry = impulse_unpacked[
                    chosen_index
                ]
                impulse_unpacked[
                    chosen_index
                ] = impulse_unpacked[
                    #impulse_unpacked
                ]
                impulse_unpacked[
                    #impulse_unpacked
                ] = nil
                local done = false
                while true do
                    if random_source:next(
                        0,
                        random_granularity - 1
                    ) >= random_granularity * chosen_entry.probability then
                        break
                    end
                    local new_pos = vector.copy(
                        pos
                    )
                    new_pos[
                        chosen_entry.axis
                    ] = new_pos[
                        chosen_entry.axis
                    ] + chosen_entry.direction
                    local new_pos_node = minetest.get_node(
                        new_pos
                    )
                    if "air" == new_pos_node.name then
                        local pos_node = minetest.get_node(
                            pos
                        )
                        local new_pos_impulse_packed = pushy.get_impulse(
                            new_pos
                        )
                        minetest.set_node(
                            new_pos,
                            pos_node
                        )
                        minetest.set_node(
                            pos,
                            new_pos_node
                        )
                        pushy.set_impulse(
                            pos,
                            new_pos_impulse_packed
                        )
                        pos = new_pos
                        done = true
                        break
                    end
                    impulse_unpacked_new[
                        #impulse_unpacked_new + 1
                    ] = {
                        probability = 1 - propagated_fraction,
                        axis = chosen_entry.axis,
                        direction = chosen_entry.direction,
                    }
                    local axes = {
                        x = true,
                        y = true,
                        z = true,
                    }
                    axes[
                        chosen_entry.axis
                    ] = nil
                    local remaining_axes = {
                    }
                    for axis, _ in pairs(
                        axes
                    ) do
                        remaining_axes[
                            #remaining_axes + 1
                        ] = axis
                    end
                    local sum = 0.0
                    local parts = {
                    }
                    for _, part in pairs(
                        deflection
                    ) do
                        local new_pos = vector.copy(
                            pos
                        )
                        new_pos[
                            chosen_entry.axis
                        ] = new_pos[
                            chosen_entry.axis
                        ] + chosen_entry.direction
                        for index, amount in pairs(
                            part.directions
                        ) do
                            local axis = remaining_axes[
                                index
                            ]
                            new_pos[
                                axis
                            ] = new_pos[
                                axis
                            ] + amount
                        end
                        local new_pos_node = minetest.get_node(
                            new_pos
                        )
                        while true do
                            local destination
                            if 0 < minetest.get_node_group(
                                new_pos_node.name,
                                "pushy"
                            ) then
                                destination = new_pos
                            elseif "air" ~= new_pos_node.name then
                                break
                            end
                            sum = sum + part.fraction
                            parts[
                                #parts + 1
                            ] = {
                                destination = destination,
                                new_direction = vector.normalize(
                                    vector.subtract(
                                        new_pos,
                                        pos
                                    )
                                ),
                                fraction = part.fraction,
                            }
                            break
                        end
                    end
                    local kept_impulse = vector.zero(
                    )
                    for _, part in pairs(
                        parts
                    ) do
                        local impulse = vector.multiply(
                            part.new_direction,
                            propagated_fraction * part.fraction / sum
                        )
                        if part.destination then
                            pushy.set_impulse(
                                part.destination,
                                vector.add(
                                    impulse,
                                    pushy.get_impulse(
                                        part.destination
                                    )
                                )
                            )
                        else
                            kept_impulse = vector.add(
                                kept_impulse,
                                impulse
                            )
                        end
                    end
                    local kept_impulse_unpacked = pushy.unpack_impulse(
                        kept_impulse
                    )
                    for _, component in pairs(
                        kept_impulse_unpacked
                    ) do
                        impulse_unpacked_new[
                            #impulse_unpacked_new + 1
                        ] = component
                    end
                    break
                end
                if done then
                    break
                end
            end
            pushy.set_impulse(
                pos,
                vector.add(
                    pushy.pack_impulse(
                        impulse_unpacked
                    ),
                    pushy.pack_impulse(
                        impulse_unpacked_new
                    )
                )
            )
        end,
    }
)

minetest.register_chatcommand(
    "set_pushy",
    {
        params = "<pos1> <pos2> <force>",
        description = "Set pushing force <force> on all pushy nodes between <pos1> and <pos2>",
        privs = {
            server = true,
        },
        func = function (
            player_name,
            param
        )
            local arguments = {
            }
            for argument in param:gmatch(
                "[^ ]+"
            ) do
                arguments[
                    #arguments + 1
                ] = argument
            end
            if 3 > #arguments then
                minetest.chat_send_player(
                    player_name,
                    modname .. ": too few parameters given"
                )
                return
            end
            if 3 < #arguments then
                minetest.chat_send_player(
                    player_name,
                    modname .. ": too many parameters given"
                )
                return
            end
            local pos1 = minetest.string_to_pos(
                arguments[
                    1
                ]
            )
            if not pos1 then
                minetest.chat_send_player(
                    player_name,
                    modname .. ": invalid position given: " .. arguments[
                        1
                    ]
                )
                return
            end
            local pos2 = minetest.string_to_pos(
                arguments[
                    2
                ]
            )
            if not pos2 then
                minetest.chat_send_player(
                    player_name,
                    modname .. ": invalid position given: " .. arguments[
                        2
                    ]
                )
                return
            end
            local force = minetest.string_to_pos(
                arguments[
                    3
                ]
            )
            if not force then
                minetest.chat_send_player(
                    player_name,
                    modname .. ": invalid direction given: " .. arguments[
                        3
                    ]
                )
                return
            end
            local step = {
            }
            for k, v in pairs(
                pos1
            ) do
                if v > pos2[
                    k
                ] then
                    step[
                        k
                    ] = -1
                else
                    step[
                        k
                    ] = 1
                end
            end
            local nodes = 0
            for x = pos1.x, pos2.x, step.x do
                for y = pos1.y, pos2.y, step.y do
                    for z = pos1.z, pos2.z, step.z do
                        local pos = {
                            x = x,
                            y = y,
                            z = z,
                        }
                        local node = minetest.get_node(
                            pos
                        )
                        if 0 < minetest.get_node_group(
                            node.name,
                            "pushy"
                        ) then
                            pushy.set_impulse(
                                pos,
                                vector.add(
                                    pushy.get_impulse(
                                        pos
                                    ),
                                    force
                                )
                            )
                            nodes = nodes + 1
                        end
                    end
                end
            end
            minetest.chat_send_player(
                player_name,
                modname .. ": force applied to " .. nodes .. " nodes"
            )
        end,
    }
)
